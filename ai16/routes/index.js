var express = require('express');
const userModel = require("../model/model");
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  if (req.session.user){
    res.redirect('/home');
  }
  else {
    res.render('index');
  }
});

// // index page



router.get('/login', function(req, res) {
  if (req.session.user){
    res.redirect('/user/' + req.session.user);
  }
  res.render('pages/login');
});

router.get('/disconnect', function(req, res) {
  if(req.session.user) {
    delete req.session.user;
  }

  res.redirect('/');
});

// Handle form submission
router.post('/login', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  // Perform the necessary login logic with the submitted username and password
  let result = userModel.checkLogin(username, password, function (result) {
    if(result == undefined){
      res.render('pages/login', { message: 'Échec de la connexion' });
    }

    else {
      req.session.user = result.id;
      res.redirect('/user/' + result.id);
    }

  });
});

router.get('/signup', function(req, res) {
  if (req.session.user){
    res.redirect('/user/' + req.session.user);
  }
  res.render('pages/signup');
});

router.post('/signup', (req, res) => {

  // Check if all required fields are filled
  if (!req.body.email || !req.body.password || !req.body.confirm_password) {
    return res.status(400).send('Please fill in all the required fields.');
  }

  // Check if passwords match
  if (req.body.password !== req.body.confirm_password) {
    return res.status(400).send('Passwords do not match.');
  }

  // TODO: Perform additional validation checks for the input data

  // TODO: Insert the user data into the database
  // You can use the database library or ORM of your choice to perform database operations

  // Assuming you are using an ORM like Sequelize, you can create a new user record
  userModel.addUser(req.body)
      .then(userID => {
        // User created successfully
        // Redirect the user to a success page or login page
        req.session.user = userID;
        res.redirect('/user/' + userID);
      })
      .catch(error => {
        // Handle any errors that occurred during user creation
        console.error(error);
        res.status(500).send('An error occurred during user signup.');
      });
});
module.exports = router;
