var express = require('express');
const userModel = require("../model/model");
var router = express.Router();

router.get('/admin', function(req, res) {
    if(!req.session.user) {
        res.redirect('/login');
    }
    else {
        res.redirect('/admin/offers')
    }
});


router.get('/admin/:subPage', async function(req, res) {
    if (!req.session.user) {
        res.redirect('/login');
    }

    const sessionID = req.session.user;
    const isRecruteur = await userModel.isRecruteur(sessionID);
    const isAdmin = await userModel.isAdmin(sessionID);

    if (!isAdmin) {
        res.redirect('/home');
    }

    const subPage = req.params.subPage;

    switch (subPage) {
        case 'offers':
            userModel.readjobs(function(result) {

                const pageSize = 3; // Number of items per page
                const currentPage = parseInt(req.query.pageNB) || 1; // Current page number
                const totalItems = result.length; // Total number of items
                const totalPages = Math.ceil(totalItems / pageSize); // Total number of pages
                const startIndex = (currentPage - 1) * pageSize; // Start index of items for the current page
                const endIndex = startIndex + pageSize; // End index of items for the current page

                res.render('pages/admin', {
                    content: "offers",
                    jobs: result.slice(startIndex, endIndex),
                    isAdmin: isAdmin,
                    isRecruteur: isRecruteur,
                    pageSize:pageSize,
                    currentPage:currentPage,
                    totalPages:totalPages,
                    totalItems:totalItems,
                });
            });
            break;

        case 'users':
            userModel.readAllUsers(function(result) {

                const pageSize = 3; // Number of items per page
                const currentPage = parseInt(req.query.pageNB) || 1; // Current page number
                const totalItems = result.length; // Total number of items
                const totalPages = Math.ceil(totalItems / pageSize); // Total number of pages
                const startIndex = (currentPage - 1) * pageSize; // Start index of items for the current page
                const endIndex = startIndex + pageSize; // End index of items for the current page


                res.render('pages/admin', {
                    content: "users",
                    users: result.slice(startIndex, endIndex),
                    isAdmin: isAdmin,
                    isRecruteur: isRecruteur,
                    pageSize:pageSize,
                    currentPage:currentPage,
                    totalPages:totalPages,
                    totalItems:totalItems,
                });
            });
            break;

        case 'orgas':
            userModel.readAllOrgas(function(result) {

                const pageSize = 3; // Number of items per page
                const currentPage = parseInt(req.query.pageNB) || 1; // Current page number
                const totalItems = result.length; // Total number of items
                const totalPages = Math.ceil(totalItems / pageSize); // Total number of pages
                const startIndex = (currentPage - 1) * pageSize; // Start index of items for the current page
                const endIndex = startIndex + pageSize; // End index of items for the current page


                res.render('pages/admin', {
                    content: "orgas",
                    orgas: result.slice(startIndex, endIndex),
                    isAdmin: isAdmin,
                    isRecruteur: isRecruteur,
                    pageSize:pageSize,
                    currentPage:currentPage,
                    totalPages:totalPages,
                    totalItems:totalItems,
                });
            });
            break;

        case 'demands':
            userModel.readAllDemandes(function(result) {

                const pageSize = 3; // Number of items per page
                const currentPage = parseInt(req.query.pageNB) || 1; // Current page number
                const totalItems = result.length; // Total number of items
                const totalPages = Math.ceil(totalItems / pageSize); // Total number of pages
                const startIndex = (currentPage - 1) * pageSize; // Start index of items for the current page
                const endIndex = startIndex + pageSize; // End index of items for the current page


                res.render('pages/admin', {
                    content: "demands",
                    demands: result.slice(startIndex, endIndex),
                    isAdmin: isAdmin,
                    isRecruteur: isRecruteur,
                    pageSize:pageSize,
                    currentPage:currentPage,
                    totalPages:totalPages,
                    totalItems:totalItems,
                });
            });
            break;

        default:
            res.redirect('/admin/offers'); // Redirect to default subpage (offers) if subPage is not recognized
    }
});

router.post('/admin/users/delete', function(req, res) {
    if(!req.session.user) {
        res.redirect('/login');
    }
    const userID = (req.query['userID'] == undefined) ? undefined : req.query['userID'];

    userModel.deleteUser(userID, function (offre) {});
    res.redirect('/admin/users')
});
router.post('/admin/orgas/delete', function(req, res) {
    if(!req.session.user) {
        res.redirect('/login');
    }
    const orgaID = (req.query['orgaID'] == undefined) ? undefined : req.query['orgaID'];

    userModel.deleteOrga(orgaID, function (offre) {});
    res.redirect('/admin/orgas')
});
router.post('/admin/offers/delete', function(req, res) {
    if(!req.session.user) {
        res.redirect('/login');
    }
    const offerID = (req.query['offerID'] == undefined) ? undefined : req.query['offerID'];

    userModel.deleteOffre(offerID, function (offre) {});
    res.redirect('/admin')
});
router.post('/admin/demands/delete', function(req, res) {
    if(!req.session.user) {
        res.redirect('/login');
    }
    const userID = (req.query['userID'] == undefined) ? undefined : req.query['userID'];

    userModel.deleteDemande(userID, function () {});
    res.redirect('/admin')
});
//


module.exports = router;