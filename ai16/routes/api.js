var express = require('express');
const userModel = require("../model/model");
var router = express.Router();

router.get('/api/users', function(req, res, next) {
    userModel.readAllUsers( function (result) {
        res.status(200).json(result);
    });
});

router.get('/api/offers', function(req, res, next) {
    userModel.readAllOffres('0', '150000', '0', '0', function (result) {
        res.status(200).json(result);
    });
});


module.exports = router;
