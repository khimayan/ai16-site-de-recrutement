var express = require('express');
const userModel = require("../model/model");
const {isRecruteur} = require("../model/model");
var router = express.Router();

router.get('/home', async function (req, res) {
    if (!req.session.user) {
        res.redirect('/login');
    }

    const sessionID = req.session.user;

    const isRecruteur = await userModel.isRecruteur(sessionID);
    const isAdmin = await userModel.isAdmin(sessionID);


    const prixMin = (req.query['salaryMin'] == undefined) ? '0' : req.query['salaryMin'];
    const prixMax = (req.query['salaryMax'] == undefined) ? '150000' : req.query['salaryMax'];
    const input_sortBy = (req.query['sortBy'] == undefined) ? '0' : req.query['sortBy'];
    const input_jobType = (req.query['jobType'] == undefined) ? '0' : req.query['jobType'];

    const offersPerPage = 3;
    let currentPage = (req.query['pageNB'] == undefined) ? '1' : req.query['pageNB'];

    const errorCode = (req.query['error'] == undefined) ? '0' : req.query['error'];


    let result = userModel.readAllOffres(prixMin, prixMax, input_jobType, input_sortBy, function (result) {
        const totalPages = Math.ceil(result.length / offersPerPage);

        currentPage = (currentPage <= totalPages && currentPage > 0) ? currentPage : 1;


        res.render('pages/home', {
            title: 'Home Page',
            salaryMin: prixMin,
            salaryMax: prixMax,
            sortBy: input_sortBy,
            jobType: input_jobType,
            Offres: result.slice((currentPage-1)*offersPerPage, (currentPage)*(offersPerPage)),
            isAdmin:isAdmin,
            isRecruteur: isRecruteur,
            currentPage: currentPage,
            totalPages: totalPages,
            errorCode:errorCode,
        })
    });
});



router.get('/candidate',function(req, res) {
    if (!req.session.user) {
        res.redirect('/login');
    }

    const sessionID = req.session.user;

    const offerID = (req.query['offerID'] == undefined) ? undefined : req.query['offerID'];

    userModel.isOfferMadeByUser(sessionID, offerID, function (result) {
        if(result.countRecruiter > 0){
            res.redirect('/home?error=1');
        }

        userModel.isUserCandidated(sessionID, offerID, function (hasCandidated) {

            if(hasCandidated){
                res.redirect('/home?error=2');
            }

            userModel.getPublicOffre(offerID, async function(Offre) {

                const isRecruteur = await userModel.isRecruteur(sessionID);
                const isAdmin = await userModel.isAdmin(sessionID);


                res.render('pages/candidate', {
                    Offre:Offre,
                    isAdmin: isAdmin,
                    isRecruteur: isRecruteur,
                })

            })
        });
    });
});

router.post('/submit_candidature',(req, res) => {
    if (req.session.user) {
        const sessionID = req.session.user;
        const bodyData = req.body;

        const offreID = req.query['offreID'];

        // Retrieve the file data
        let cvData = undefined;
        let lettreData = undefined;
        let autreData = undefined;

        // Check if the CV file is present
        if (req.files && req.files.cv) {
            cvData = req.files.cv.data;
            // Process the CV data here
        }

// Check if the lettre_motivation file is present
        if (req.files && req.files.lettre_motivation) {
            lettreData = req.files.lettre_motivation.data;
            // Process the lettre_motivation data here
        }

// Check if the autre_fichier file is present
        if (req.files && req.files.autre_fichier) {
            autreData = req.files.autre_fichier.data;
            // Process the autre_fichier data here
        }

        userModel.addCandidature(bodyData, cvData, lettreData, autreData, sessionID, offreID)
        res.redirect('/home?error=3');
    } else {
        // Handle the case when req.session.user does not exist
        res.redirect('/login'); // Redirect to login page or appropriate error handling
    }
});

router.post('/update_user', function(req, res) {
    if (req.session.user) {
        const userData = req.body;
        const session_id = req.session.user;
        userModel.setUser(userData, session_id);
        res.redirect('/user');
    }
    else {
        // Handle the case when req.session.user does not exist
        res.redirect('/login'); // Redirect to login page or appropriate error handling
    }
});


module.exports = router;

