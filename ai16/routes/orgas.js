var express = require('express');
const userModel = require("../model/model");
var router = express.Router();

router.get('/organisations', async function (req, res, next) {

    const sessionID = req.session.user;
    const isRecruteur = await userModel.isRecruteur(sessionID);
    const isAdmin = await userModel.isAdmin(sessionID);
    const errorCode = (req.query['error'] === undefined) ? '0' : req.query['error'];
    if (sessionID === undefined) {
        res.redirect('/login');
    } else {
        userModel.readAllOrgas(function (orgas) {
            res.render('pages/orgas', {
                orgas: orgas, isAdmin: isAdmin, isRecruteur: isRecruteur,
                errorCode: errorCode
            });
        })
    }
});

router.post('/organisations/new', function(req, res) {
    const userID = req.session.user;
    if(!req.session.user) {
        res.redirect('/organisations');
    }
    const orgaID = (req.query['orgaID'] == undefined) ? undefined : req.query['orgaID'];


    userModel.demandExists(userID, orgaID, function (exists) {
        if (exists) {
            res.redirect('/organisations?error=1')
            console.log(exists)
        }
        else {
            res.redirect('/organisations?error=2')
            userModel.addDemande(userID, orgaID, function (offre) {});
            console.log(exists)
        }
    });
});

module.exports = router;