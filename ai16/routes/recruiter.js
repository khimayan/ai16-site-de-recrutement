var express = require('express');
const userModel = require("../model/model");
var router = express.Router();


router.get('/recruiter',function(req, res) {
    res.redirect('/recruiter/offers');
});

router.get('/recruiter/:subPage', async function (req, res) {
    if (!req.session.user) {
        res.redirect('/login');
    }

    const sessionID = req.session.user;

    const offerID = (req.query['offerID'] == undefined) ? undefined : req.query['offerID'];

    const isRecruteur = await userModel.isRecruteur(sessionID);
    const isAdmin = await userModel.isAdmin(sessionID);

    if(!isRecruteur){
        res.redirect('/home');
    }

    const subPage = req.params.subPage;

    switch (subPage) {
        case 'offers':
            userModel.readRecruiterOffres(sessionID, function (offres) {

                const pageSize = 2; // Number of items per page
                const currentPage = parseInt(req.query.pageNB) || 1; // Current page number
                const totalItems = offres.length; // Total number of items
                const totalPages = Math.ceil(totalItems / pageSize); // Total number of pages
                const startIndex = (currentPage - 1) * pageSize; // Start index of items for the current page
                const endIndex = startIndex + pageSize; // End index of items for the current page

                res.render('pages/recruiter', {
                    subPageType: subPage,
                    offres: offres.slice(startIndex, endIndex),
                    isAdmin: isAdmin,
                    isRecruteur: isRecruteur,
                    pageSize:pageSize,
                    currentPage:currentPage,
                    totalPages:totalPages,
                    totalItems:totalItems,
                });
            });
            break;

        case 'edit':
            if (offerID === undefined) {
                res.redirect('/recruiter')
            }
            userModel.getOffre(sessionID, offerID, function (offre) {
                res.render('pages/recruiter', {
                    subPageType: subPage,
                    offre: offre,
                    isAdmin: isAdmin,
                    isRecruteur: isRecruteur,
                });
            });
            break;

        case 'delete':
            if (offerID === undefined) {
                res.redirect('/recruiter')
            }
            userModel.deleteOffre(offerID, function (offre) {
            });
            res.redirect('/recruiter')
            break;

        case 'show':
            if (offerID === undefined) {
                res.redirect('/recruiter')
            }
            userModel.getCandidates(offerID, sessionID, function (candidates) {
                userModel.getOffre(sessionID, offerID, function (offre) {
                    res.render('pages/recruiter', {subPageType: subPage, offre: offre, candidates: candidates, isAdmin: isAdmin, isRecruteur: isRecruteur});
                })
            });
            break;

        case 'orga':
            userModel.getOrga(sessionID, function (orga) {
                res.render('pages/recruiter', {subPageType: subPage, orga: orga, isAdmin: isAdmin, isRecruteur: isRecruteur,});
            });
            break;

        case 'add':
            res.render('pages/recruiter', {subPageType: subPage, isAdmin: isAdmin, isRecruteur: isRecruteur,});
            break;

        case 'demands':
            userModel.getOrga(sessionID, function (orga) {
                userModel.getDemands(orga.id, function(recruteurs){
                    const pageSize = 3; // Number of items per page
                    const currentPage = parseInt(req.query.pageNB) || 1; // Current page number
                    const totalItems = recruteurs.length; // Total number of items
                    const totalPages = Math.ceil(totalItems / pageSize); // Total number of pages
                    const startIndex = (currentPage - 1) * pageSize; // Start index of items for the current page
                    const endIndex = startIndex + pageSize; // End index of items for the current page
                    res.render('pages/recruiter', {subPageType: subPage, orga: orga, recruteurs : recruteurs,
                        isAdmin: isAdmin, isRecruteur: isRecruteur, currentPage: currentPage,
                        totalPages: totalPages, totalItems: totalItems,pageSize: pageSize,
                        startIndex: startIndex, endIndex: endIndex});
                })
            })
            break;
    }
});
router.post('/recruiter/demands/delete', function(req, res){
    const recruteurID = (req.query['recruteurID'] === undefined) ? undefined : req.query['recruteurID'];
    if (recruteurID === undefined) {
        res.redirect('/recruiter/demands')
    }
    userModel.deleteDemande(recruteurID, function (offre) {
    });
    res.redirect('/recruiter/demands')
})

router.post('/recruiter/demands/delete', function(req, res){
    const recruteurID = (req.query['recruteurID'] === undefined) ? undefined : req.query['recruteurID'];
    if (recruteurID === undefined) {
        res.redirect('/recruiter/demands')
    }
    userModel.deleteDemande(recruteurID, function (offre) {
    });
    res.redirect('/recruiter/demands')
})

router.post('/recruiter/demands/accept', function(req, res){
    const recruteurID = (req.query['recruteurID'] === undefined) ? undefined : req.query['recruteurID'];
    const sessionID = req.session.user;
    userModel.getOrga(sessionID, function (orga){
        if (recruteurID === undefined) {
            res.redirect('/recruiter/demands')
        }
        userModel.acceptDemande(recruteurID, orga.id, function (offre) {
            res.redirect('/recruiter/demands')
        });

    })

})


router.post('/update_offer', function(req, res) {
    if (req.session.user) {
        const recruiterID = req.session.user;
        const offreData = req.body;
        const offerID = req.query['offerID'];

        userModel.setOffre(offreData, offerID, recruiterID);

        res.redirect('/recruiter/edit?offerID=' + offerID);
    } else {
        // Handle the case when req.session.user does not exist
        res.redirect('/login'); // Redirect to login page or appropriate error handling
    }
});

router.post('/update_orga', function (req, res) {
    if (req.session.user) {
        const recruiterID = req.session.user;
        const orgaData = req.body;
        const orgaID = req.query['orgaID'];

        userModel.setOrga(orgaData, orgaID, recruiterID);

        res.redirect('/recruiter');
    } else {
        // Handle the case when req.session.user does not exist
        res.redirect('/login'); // Redirect to login page or appropriate error handling
    }
});

router.post('/add_offer', function (req, res) {
    if (req.session.user) {
        const recruiterID = req.session.user;
        const offreData = req.body;
        userModel.addOffre(offreData, recruiterID);

        res.redirect('/recruiter');
    } else {
        // Handle the case when req.session.user does not exist
        res.redirect('/login'); // Redirect to login page or appropriate error handling
    }
});

module.exports = router;

