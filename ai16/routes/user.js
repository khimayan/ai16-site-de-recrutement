var express = require('express');
const userModel = require("../model/model");
const {isRecruteur} = require("../model/model");
var router = express.Router();

router.get('/user', function(req, res, next) {
    if(!req.session.user) {
        res.redirect('/login');
    }
    else{
        res.redirect('/user/' + req.session.user);
    }
});


router.get('/user/:id', async function (req, res) {
    if (!req.session.user) {
        res.redirect('/login');
    }

    const sessionID = req.session.user;
    const isRecruteur = await userModel.isRecruteur(sessionID);
    const isAdmin = await userModel.isAdmin(sessionID);


    userModel.getUser(req.params.id, function (current_user) {

        userModel.getUser(req.session.user, function (session_user) {
            if (req.query['showProfile'] == undefined) {
                res.render('pages/user', {current_user: current_user, session_user: session_user, isAdmin:isAdmin, isRecruteur: isRecruteur,});
            } else {
                res.render('pages/user', {current_user: current_user, session_user: -1, isAdmin:isAdmin, isRecruteur: isRecruteur,});
            }
        });
    });
});



module.exports = router;

