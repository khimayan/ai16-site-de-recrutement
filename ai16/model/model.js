var db = require('./db.js');
const bcrypt = require('bcrypt');


module.exports = {

    readAllUsers: function (callback) {
        db.query("select * from Utilisateur", function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
    readAllOrgas: function (callback) {
        db.query("select * from Organisation", function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
    readjobs: function (callback) {
        db.query("select * from Offre", function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },

    getUser: function (user_id, callback) {
        sql = "SELECT * FROM Utilisateur WHERE id = ?";
        rows = db.query(sql, user_id, function (err, results) {
            if (err) throw err;
            callback(results[0]);
        });
    },

    getCandidates: function(offerID, sessionID, callback) {
        const sql = `SELECT C.id AS candidature_id, C.contenu, C.cv, C.lettre_motivation, C.autre_fichier, U.id AS candidat_id, U.nom, U.prenom, U.numero, U.mail, U.ville, U.pays, R.idUtilisateur AS recruteur_id
    FROM Candidature C
    JOIN Offre O ON C.idOffre = O.id
    JOIN Utilisateur U ON C.idCandidat = U.id
    JOIN Recruteur R ON O.idRecruteur = R.idUtilisateur
    WHERE O.id = ? AND O.idRecruteur = ?`;

        db.query(sql, [offerID, sessionID], function(err, results) {
            if (err) throw err;
            callback(results);
        });
    },

    //SHOULD NOT BE USED DIRECTLY
    getIsRecruteur: function (sessionID) {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT * FROM Recruteur WHERE idUtilisateur = ?';
            db.query(sql, [sessionID], function (err, results) {
                if (err) {
                    reject(err);
                } else {
                    resolve(results.length > 0);
                }
            });
        });
    },



    isRecruteur : async function (sessionID) {
        let isRecruteur = undefined;
        await this.getIsRecruteur(sessionID)
            .then(result => {
                isRecruteur = result;
            })
            .catch(err => {
                console.error("Error:", err);
            });

        return isRecruteur;
    },

    //SHOULD NOT BE USED DIRECTLY
    getIsAdmin: function (sessionID) {
        return new Promise((resolve, reject) => {
            const sql = 'SELECT * FROM Admin WHERE idUtilisateur = ?';
            db.query(sql, [sessionID], function (err, results) {
                if (err) {
                    reject(err);
                } else {
                    resolve(results.length > 0);
                }
            });
        });
    },

    isAdmin : async function (sessionID) {
        let isAdmin = undefined;
        await this.getIsAdmin(sessionID)
            .then(result => {
                isAdmin = result;
            })
            .catch(err => {
                console.error("Error:", err);
            });

        return isAdmin;
    },

    getOrga: function (sessionID, callback) {
        sql = "SELECT * FROM Organisation O JOIN Recruteur U ON U.idOrganisation = O.id AND U.idUtilisateur = ?"
        rows = db.query(sql, sessionID, function (err, results) {
            if (err) throw err;
            callback(results[0]);
        });
    },

    getDemands: function(orgaID, callback) {
        sql = "SELECT r.*\n" +
            "FROM Utilisateur r\n" +
            "INNER JOIN Demande_Orga d ON r.id = d.idUtilisateur\n" +
            "WHERE d.idOrganisation = ?;\n"
        rows = db.query(sql, orgaID, function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },

    setOffre: function(body, offerID, recruiterID) {
        const offerData = body;
        const sql = `
    UPDATE Offre
    SET intitule    = ?,
        description = ?,
        offre_type  = ?,
        etat        = ?,
        prix        = ?,
        creation    = ?
    WHERE id = ?
      AND idRecruteur = ?`;

        const values = [
            offerData.intitule,
            offerData.description,
            offerData.offre_type,
            offerData.etat,
            offerData.prix,
            offerData.creation,
            offerID,
            recruiterID
        ];

        db.query(sql, values, function(err, result) {
            if (err) {
                console.error(err);
                return;
            }
            // Handle the success or result of the update operation here
        });
    },


    addOffre: function(body, recruiterID) {
        const offerData = body;
        const sql = `
    INSERT INTO Offre(intitule, description, offre_type, etat, prix, creation, idRecruteur)
    VALUES (?, ?, ?, ?, ?, ?, ?)`;

        const values = [
            offerData.intitule,
            offerData.description,
            offerData.offre_type,
            offerData.etat,
            offerData.prix,
            offerData.creation,
            recruiterID
        ];

        db.query(sql, values, function(err, result) {
            if (err) {
                console.error(err);
                return;
            }
            // Handle the success or result of the insert operation here
        });
    },

    addCandidate: function (body, sessionID) {
        const offerData = body;
        const sql = `
    INSERT INTO Offre(intitule, description, offre_type, etat, prix, creation, idRecruteur)
    VALUES (?, ?, ?, ?, ?, ?, ?);
  `;
        const values = [
            offerData.intitule,
            offerData.description,
            offerData.offre_type,
            offerData.etat,
            offerData.prix,
            offerData.creation,
            recruiterID
        ];

        db.query(sql, values, function (err, result) {
            if (err) {
                console.error(err);
            }
            // Handle the success or result of the update operation here
        });
    },

    addUser: function (body) {
        const userData = body;
        const saltRounds = 10; // Number of salt rounds for bcrypt hashing
        const sql = `
    INSERT INTO Utilisateur(nom, prenom, numero, mail, mdp, creation, status, ville, pays, description, facebook, twitter, instagram, linkedin)
    VALUES (?, ?, ?, ?, ?, NOW(), 1, ?, ?, ?, ?, ?, ?, ?);
  `;
        const values = [
            userData.nom,
            userData.prenom,
            userData.numero,
            userData.email,
            null,
            userData.ville,
            userData.pays,
            userData.description,
            userData.facebook,
            userData.twitter,
            userData.instagram,
            userData.linkedin
        ];


        return new Promise((resolve, reject) => {
            bcrypt.genSalt(saltRounds, function (err, salt) {
                if (err) {
                    console.error(err);
                    reject(err);
                    return;
                }

                bcrypt.hash(userData.password, salt, function (err, hashedPassword) {
                    if (err) {
                        console.error(err);
                        reject(err);
                        return;
                    }

                    values[4] = hashedPassword; // Set the hashed password in the values array

                    db.query(sql, values, function (err, result) {
                        if (err) {
                            console.error(err);
                            reject(err);
                        } else {
                            const insertedUserId = result.insertId;
                            resolve(insertedUserId);
                        }
                    });
                });
            });
        });
    },

    deleteOffre: function (offreID) {
        const deleteCandidatureSql = 'DELETE FROM Candidature WHERE idOffre = ?';
        const deleteOffreSql = 'DELETE FROM Offre WHERE id = ?';

        db.query(deleteCandidatureSql, [offreID], function (err, result) {
            if (err) {
                console.error(err);
            }
            // Handle the success or result of the delete operation here
            db.query(deleteOffreSql, [offreID], function (err, result) {
                if (err) {
                    console.error(err);
                }
                // Handle the success or result of the delete operation here
            });
        });
    },
    addDemande: function (recruteurID, orgaID) {
        const addDemandeSql = 'INSERT INTO Demande_Orga (idUtilisateur, idOrganisation) VALUES (?, ?)';
        const values = [recruteurID, orgaID];

        db.query(addDemandeSql, values, function (err, result) {
            if (err) {
                console.error(err);
            }
            // Handle the success or result of the insert operation here
        });
    },

    deleteDemande: function (recruteurID) {
        const deleteDemandeSql = 'DELETE FROM Demande_Orga WHERE idUtilisateur = ?';

        db.query(deleteDemandeSql, [recruteurID], function (err, result) {
            if (err) {
                console.error(err);
            }
            // Handle the success or result of the delete operation here
        });
    },

    acceptDemande: function (recruteurID, orgaID) {
        const addRecruteurSql = 'INSERT INTO Recruteur (idUtilisateur, idOrganisation) VALUES (?, ?)';
        const deleteDemandeSql = 'DELETE FROM Demande_Orga WHERE idUtilisateur = ?';
        const valuesAddRecruteur = [recruteurID, orgaID];
        const valuesDeleteDemande = [recruteurID];

        db.query(addRecruteurSql, valuesAddRecruteur, function (err, result) {
            if (err) {
                console.error(err);
            }
            // Handle the success or result of the insert operation here
        });

        db.query(deleteDemandeSql, valuesDeleteDemande, function (err, result) {
            if (err) {
                console.error(err);
            }
            // Handle the success or result of the delete operation here
        });
    },

    deleteUser: function (userID) {
        const deleteOffreSql = `
    DELETE o
    FROM Offre o
    JOIN Recruteur r ON o.idRecruteur = r.idUtilisateur
    WHERE r.idUtilisateur = ?;
  `;
        const deleteCandidatureSql = `
    DELETE FROM Candidature
    WHERE idCandidat = ?;
  `;
        const deleteAdminSql = `
    DELETE FROM Admin
    WHERE idUtilisateur = ?;
  `;
        const deleteRecruteurSql = `
    DELETE FROM Recruteur
    WHERE idUtilisateur = ?;
  `;
        const deleteUserSql = 'DELETE FROM Utilisateur WHERE id = ?;';

        db.query(deleteOffreSql, [userID], function (err, result) {
            if (err) {
                console.error(err);
            }
            db.query(deleteCandidatureSql, [userID], function (err, result) {
                if (err) {
                    console.error(err);
                }
                db.query(deleteAdminSql, [userID], function (err, result) {
                    if (err) {
                        console.error(err);
                    }
                    db.query(deleteRecruteurSql, [userID], function (err, result) {
                        if (err) {
                            console.error(err);
                        }
                        // Handle the success or result of the delete operation here
                        db.query(deleteUserSql, [userID], function (err, result) {
                            if (err) {
                                console.error(err);
                            }
                            // Handle the success or result of the delete operation here
                        });
                    });
                });
            });
        });
    },

    deleteOrga: function (orgaID) {
        const deleteOffreSql = `
    DELETE FROM Offre
    WHERE idRecruteur IN (
        SELECT idUtilisateur
        FROM Recruteur
        WHERE idOrganisation = ?
    );
  `;
        const deleteCandidatureSql = `
    DELETE FROM Candidature
    WHERE idOffre IN (
        SELECT id
        FROM Offre
        WHERE idRecruteur IN (
            SELECT idUtilisateur
            FROM Recruteur
            WHERE idOrganisation = ?
        )
    );
  `;
        const deleteRecruteurSql = `
    DELETE FROM Recruteur
    WHERE idOrganisation = ?;
  `;
        const deleteOrgaSql = `
    DELETE FROM Organisation
    WHERE id = ?;
  `;

        db.query(deleteOffreSql, [orgaID], function (err, result) {
            if (err) {
                console.error(err);
            }
            db.query(deleteCandidatureSql, [orgaID], function (err, result) {
                if (err) {
                    console.error(err);
                }
                db.query(deleteRecruteurSql, [orgaID], function (err, result) {
                    if (err) {
                        console.error(err);
                    }
                    db.query(deleteOrgaSql, [orgaID], function (err, result) {
                        if (err) {
                            console.error(err);
                        }
                    });
                });
            });
        });
    },

    setOrga: function (body, orgaID, recruiterID) {
        const orgaData = body;
        const sql = `
    UPDATE Organisation 
    SET nom = ?,
        description = ?,
        type = ?,
        siren = ?,
        siege_social = ?
    WHERE id = ?`;

        const values = [
            orgaData.nom,
            orgaData.description,
            orgaData.type,
            orgaData.siren,
            orgaData.siege,
            orgaID
        ];

        db.query(sql, values, function (err, result) {
            if (err) {
                console.error(err);
                return;
            }
            // Handle the success or result of the update operation here
        });
    },

    setUser: function (body, user_id) {
        const userData = body;
        const sql = `
    UPDATE Utilisateur
    SET nom = ?,
        prenom = ?,
        numero = ?,
        mail = ?,
        description = ?,
        ville = ?,
        pays = ?,
        facebook = ?,
        twitter = ?,
        instagram = ?,
        linkedin = ?
    WHERE id = ?`;

        const values = [
            userData.lastName,
            userData.firstName,
            userData.numero,
            userData.email,
            userData.description,
            userData.ville,
            userData.pays,
            userData.facebook,
            userData.twitter,
            userData.instagram,
            userData.linkedin,
            user_id
        ];

        db.query(sql, values, function (err, result) {
            if (err) {
                console.error(err);
                return;
            }
            // Handle the success or result of the update operation here
        });
    },

    checkLogin: function (username, password, callback) {

        console.log(username, password)

        const sql = "SELECT * FROM Utilisateur WHERE mail = ?";
        const values = [username];

        db.query(sql, values, function (err, results) {
            if (err) throw err;

            if (results.length === 0) {
                // User not found
                callback(undefined);
                return;
            }

            const user = results[0];

            // Compare the submitted password with the stored hashed password
            bcrypt.compare(password, user.mdp, (err, isMatch) => {
                if (err) throw err;

                if (!isMatch) {
                    // Password does not match
                    callback(undefined);
                    return;
                }

                // Password matches
                callback(user);
            });
        });
    },


    readRecruiterOffres: function (recruiterID, callback) {
        const sql = `SELECT *
               FROM Offre
               WHERE idRecruteur = ?`;

        // Execute the SQL query to retrieve offres for the specified recruiter
        db.query(sql, recruiterID, function (err, results) {
            if (err) {
                console.error(err);
                return;
            }

            // Pass the retrieved offres to the callback function
            callback(results);
        });
    },

    getOffre: function (recruiterID, offerID, callback) {
        const sql = `SELECT *
               FROM Offre
               WHERE idRecruteur = ?
                 AND id = ?`;

        const values = [recruiterID, offerID];

        // Execute the SQL query to retrieve offres for the specified recruiter
        db.query(sql, values, function (err, results) {
            if (err) {
                console.error(err);
                return;
            }

            // Pass the retrieved offres to the callback function
            callback(results[0]);
        });
    },

    getPublicOffre: function (offerID, callback) {
        const sql = `SELECT *
               FROM Offre
               WHERE etat = 'publiée'
                 AND id = ?`;

        // Execute the SQL query to retrieve offres for the specified recruiter
        db.query(sql, offerID, function (err, results) {
            if (err) {
                console.error(err);
                return;
            }

            // Pass the retrieved offres to the callback function
            callback(results[0]);
        });
    },


    isOfferMadeByUser: function (sessionID, offerID, callback) {
        const sql = `
    SELECT COUNT(*) AS countRecruiter
    FROM Recruteur
    JOIN Offre ON Recruteur.idUtilisateur = Offre.idRecruteur
    WHERE Recruteur.idUtilisateur = ?
      AND Offre.id = ?`;
        const values = [sessionID, offerID];
        // Execute the SQL query to retrieve offres for the specified recruiter
        db.query(sql, values, function (err, results) {
            if (err) {
                console.error(err);
                return;
            }
            // Pass the retrieved offres to the callback function
            callback(results[0]);
        });
    },

    isUserCandidated: function (sessionID, offerID, callback) {
        const sql = `
    SELECT COUNT(*) AS countCandidacy
    FROM Candidature
    WHERE idCandidat = ?
      AND idOffre = ?`;

        const values = [sessionID, offerID];

        // Execute the SQL query to check if the user has already submitted a candidacy
        db.query(sql, values, function (err, results) {
            if (err) {
                console.error(err);
                return;
            }

            // Pass the result indicating if the user has candidated to the callback function
            callback(results[0].countCandidacy > 0);
        });
    },

    demandExists: function(sessionID, orgaID, callback) {
        const sql = `SELECT COUNT(*) AS existing_requests
                     FROM Demande_Orga
                     WHERE idUtilisateur = ? AND idOrganisation = ?; `
        values = [sessionID, orgaID];
        db.query(sql, values, function(err, results){
                if (err) throw err;
                callback(results[0].existing_requests > 0);
            });
    },

    readAllDemandes: function(callback) {
        db.query("SELECT D.id AS demande_id, U.id AS utilisateur_id, U.nom AS utilisateur_nom, U.prenom AS utilisateur_prenom, U.mail AS utilisateur_mail, O.nom AS organisation_nom\n" +
            "FROM Demande_Orga D\n" +
            "JOIN Utilisateur U ON D.idUtilisateur = U.id\n" +
            "JOIN Organisation O ON D.idOrganisation = O.id;\n", function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },
    readAllOffres: function (prixMin, prixMax, offre_type, sort_type, callback) {
        const conditions = [];
        const params = [];

        conditions.push('o.prix BETWEEN ? AND ?');
        params.push(prixMin, prixMax);

        switch (offre_type) {
            case '1':
                conditions.push('o.offre_type = ?');
                params.push('Temps Plein');
                break;
            case '2':
                conditions.push('o.offre_type = ?');
                params.push('Temps Partiel');
                break;
            case '3':
                conditions.push('o.offre_type = ?');
                params.push('Interim');
                break;
            case '4':
                conditions.push('o.offre_type = ?');
                params.push('Stage');
                break;
        }

        conditions.push('o.etat = \'publiée\'');

        const sort_type_string =
            sort_type === '1'
                ? 'o.creation ASC'
                : sort_type === '2'
                    ? 'o.prix ASC'
                    : sort_type === '3'
                        ? 'o.prix DESC'
                        : 'o.creation DESC';

        const query = `
    SELECT o.*, r.prenom AS recruiter_prenom, r.nom AS recruiter_nom, org.nom AS organization_name
    FROM Offre o
    INNER JOIN Recruteur rec ON o.idRecruteur = rec.idUtilisateur
    INNER JOIN Utilisateur r ON rec.idUtilisateur = r.id
    INNER JOIN Organisation org ON rec.idOrganisation = org.id
    WHERE ${conditions.join(' AND ')}
    ORDER BY ${sort_type_string}
  `;

        db.query(query, params, function (err, results) {
            if (err) throw err;
            callback(results);
        });
    },

    addCandidature: function (body, cv, lettre_motivation, autre, sessionID, offreID) {
        const candidatureData = body;

        const sql = `
    INSERT INTO Candidature(contenu, cv, lettre_motivation, autre_fichier, idOffre, idCandidat)
    VALUES (?, ?, ?, ?, ?, ?);`;

        const values = [
            candidatureData.contenu,
            cv || null,
            lettre_motivation || null,
            autre || null,
            offreID,
            sessionID,
        ];

        db.query(sql, values, function (err, result) {
            if (err) {
                console.error(err);
            }
            // Handle the success or result of the insert operation here
        });
    },
}


