
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var escapeHtml = require('escape-html');
const fileUpload = require('express-fileupload');
const fs = require('fs');
const bcrypt = require('bcrypt');

var cors = require('cors');

var express = require('express');
var router = express.Router();

const routes = {
  indexRouter: './routes/index',
  homeRouter: './routes/home',
  usersRouter: './routes/user',
  adminRouter: './routes/admin',
  recRouter: './routes/recruiter',
  orgasRouter : '/routes/orgas',
  apiRouter : '/routes/api'
  // Add more routes here as needed
};

var app = express();

require('dotenv').config();

//session setup
app.use(session({
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: true,
  name: 'session cookie', // Customise the name
  cookie: { secure: false , maxAge: 600000} // set to true if using HTTPS; maxAge in ms (600000 ms = 10 min)
}))

app.use(fileUpload());

app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Require all routes
for (const key in routes) {
  if (routes.hasOwnProperty(key)) {
    const routePath = path.join(__dirname, routes[key]);
    const route = require(routePath);
    app.use('/', route);
  }
}

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(8080);

module.exports = app;
