
# AI16 - Site de recrutement 

Ce fichier regroupe la structure ainsi que les différentes ressources menant à bien ce projet 

## Auteurs

- [Nicola Milani](https://gitlab.utc.fr/milanini)

- [Yaniss Khima](https://gitlab.utc.fr/khimayan)


## Fonctionnement

Clone du projet

```bash
  git clone https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement.git
  cd ai16-site-de-recrutement/ai16
```

Installation des dépendances

```bash
  npm install
```

Configuration du fichier environnement

```bash
  nano ./template.env
  mv ./template.env ./.env
```


Lancer le serveur

```bash
  /usr/bin/node app.js
```

Le serveur est accessible à l'adresse 

```bash
    localhost:8080
```

## TD Rest sous Vue.JS

Installation des dépendances

```bash
  cd ai16-site-de-recrutement/TD_Rest
  npm install
```

Lancer le serveur

```bash
  npm run dev
```

Le serveur est accessible à l'adresse 

```bash
    localhost:5173
```

Attention à avoir bien lancé le serveur Node pour pouvoir communiquer avec l'API

<br>

<details>
<summary>Capture d'écran du résultat</summary>

![Page d'accueil](Doc/td_rest_screens/Screenshot%202023-06-19%20at%2017-56-52%20SR10.png)

</details>

<br>

## Architecture du repertoire git

L'architecture de dossier vous est présenté ci-dessous
    
- ai16
  - bin/
    - [www](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/blob/main/ai16/bin/www) - Configuration du serveur web
  - model/
    - [db.js](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/blob/main/ai16/model/db.js) - Configuration MySQL
    - [model.js](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/blob/main/ai16/model/model.js) - Méthodes de communication avec la base de données
  - [routes](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/blob/main/ai16/routes/) - Routage des différentes pages
  - views/
    - [pages/](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/blob/main/ai16/views/pages/) - Pages EJS principales
    - [partials/](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/blob/main/ai16/views/partials/) - Composants communs et sous-pages
  - [app.js](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/blob/main/ai16/app.js) - Point d'entré du serveur web
  - [template.env](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/blob/main/ai16/template.env) - Modèle de fichier d'environement
- Doc
  - [database.SQL](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/blob/main/Doc/database.SQL) - Modèle physique des données
  - [MLD.txt](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/blob/main/Doc/MLD.txt) - Modèle logique de données
  - [Usecase.png](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/blob/main/Doc/Usecase.png) - Diagramme de cas d'utilisations
- [Templates/](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/blob/main/Templates/) - Maquettes HTML statiques

- [TD_Rest/](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/blob/main/TD_Rest/) - TD_Rest sous Vue.JS


## Comparaison MCD face au MPD

<br>

<details>
<summary>Modèle conceptuel des données</summary>

![MCD](Doc/BDD_Relationnel.png)

</details>

<br>


<details>
<summary>Modèle physique des données</summary>

![MPD](Doc/ai16p006.drawio.png)

</details>

<br>

Dans le cadre du développement des interactions avec l'API, nous avons apporté des modifications significatives à la base de données afin de simplifier son architecture. Dans cette optique, nous avons supprimé les tables "piece" et "piece_demande", qui n'étaient plus nécessaires.

De plus, nous avons ajouté des clés supplémentaires à la table "utilisateur" afin de fournir une page utilisateur plus complète. Ces modifications ont été effectuées dans le but de faciliter le développement des fonctionnalités liées à l'API.

Cependant, à grande échelle, il serait judicieux de considérer une séparation entre l'utilisateur et son contenu de la base de données, tels que les pièces jointes ou les CV. Cette séparation permettrait une meilleure optimisation de la base de données, en réduisant les duplications de données et en améliorant les performances globales du système.

## Tableau des utilisateurs
Le MPD [database.SQL](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/blob/main/Doc/database.SQL) fourni une banque de données par défaut pour pouvoir naviguer et tester plus facilement le projet, les mot de passes étant chiffré, voici la liste des utilisateurs :

| Rôle                     | Email                    | Mot de passe |
|--------------------------|--------------------------|--------------|
| Recruteur/Administrateur | jean.dupont@example.com  | 123456       |
| Recruteur/Administrateur | marie.martin@example.com | azerty       |
| Utilisateur              | pierre.leroy@example.com | motdepasse   |
| Utilisateur              | manuel.garcia@mail.com   | azerty123    |


## Captures des pages principales 

<br>

<details>
<summary>Page d'accueil</summary>

![Page d'accueil](Doc/project_screens/Screenshot%202023-06-17%20at%2022-36-43%20Page%20d'accueil.png)

</details>

<br>

<details>
<summary>Page Utilisateur</summary>

![Page d'accueil](Doc/project_screens/Screenshot%202023-06-17%20at%2022-39-10%20User%20Page.png)

</details>

<br>

<details>
<summary>Liste Des Offres</summary>

![Page d'accueil](Doc/project_screens/Screenshot%202023-06-17%20at%2022-39-22%20Accueil.png)

</details>

<br>


<details>
<summary>Page de candidature</summary>

![Page d'accueil](Doc/project_screens/Screenshot%202023-06-17%20at%2022-45-39%20Candidater.png)

</details>

<br>

<details>
<summary>Page Recruteur</summary>

![Page d'accueil](Doc/project_screens/Screenshot%202023-06-17%20at%2022-41-35%20Page%20Recruteur.png)

</details>

<br>

<details>
<summary>Page Administrateur</summary>

![Page d'accueil](Doc/project_screens/Screenshot%202023-06-17%20at%2022-44-24%20Page%20Administrateur.png)

</details>

<br>

## Rapport sur la sécrité web

Nous avons identifié trois axes importants pour prévenir les failles de sécurité web : 

<br>

L'utilisation de dotenv nous permet de stocker les informations sensibles, telles que les identifiants et les mots de passe, dans un fichier d'environnement séparé du code source. Cela offre plusieurs avantages en termes de sécurité. Tout d'abord, en séparant les crédentials du code source, nous réduisons les risques de divulgation accidentelle ou malveillante de ces informations. De plus, en utilisant un fichier d'environnement, nous pouvons facilement modifier les crédentials sans avoir à modifier le code lui-même, ce qui simplifie la gestion des configurations sensibles.

Vous pouvez voir le commit impliqué [ici](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/commit/e2537d2a26683f5d16b4a6395962555267af91ec)

<br>

Pour prévenir les attaques par injection SQL, nous avons adopté une approche sécurisée en utilisant les fonctions de requête de la base de données avec des arguments en paramètres représentés par des points d'interrogation (?). Cette technique permet de séparer les données fournies par l'utilisateur des requêtes SQL, réduisant ainsi le risque d'injection de code malveillant. Voici un exemple : 

```js
getOffre: function (recruiterID, offerID, callback) {
    const sql = `SELECT *
        FROM Offre
        WHERE idRecruteur = ${recruiterID}
          AND id = ${offerID}`;

    // Execute the SQL query to retrieve offres for the specified recruiter
    db.query(sql, function (err, results) {
        if (err) {
            console.error(err);
            return;
        }

        // Pass the retrieved offres to the callback function
        callback(results[0]);
    });
},

```

Qui a été changé en :

```js
getOffre: function (recruiterID, offerID, callback) {
    const sql = `SELECT *
        FROM Offre
        WHERE idRecruteur = ?
        AND id = ?`;

    const values = [recruiterID, offerID];

    // Execute the SQL query to retrieve offres for the specified recruiter
    db.query(sql, values, function (err, results) {
        if (err) {
            console.error(err);
            return;
        }

        // Pass the retrieved offres to the callback function
        callback(results[0]);
    });
},
```

Vous pouvez voir le commit impliqué [ici](https://gitlab.utc.fr/khimayan/ai16-site-de-recrutement/-/commit/a6c4affcc465a2a414e373cf049de7f69d2ce319#44ea7ff6fdb5471e1136cf2d5e8491e1c627e30b_467_510)

<br>

Le troisième et dernier axe que nous avons choisi pour prévenir les failles de sécurité web concerne la sécurisation des mots de passe utilisateurs. Nous avons mis en place l'utilisation de la fonction de hachage bcrypt pour stocker les mots de passe de manière sécurisée, sans les conserver en clair.


